---
layout: pages/post.pug
title: REST Api with passport authentication
category: tutorial
description: Application Program Interfaces, APIs, are snippets of code that allow one software application to talk to another, providing a common language. Whether allowing seamless experiences for end users across multiple applications, or allowing data from one application to be fed into another, APIs have revolutionised in the last years.
unsplashImageId: VybTLOKwlPc
---
[REST Api with passport authentication](https://medium.com/@abdoumjr/laravel-passport-create-rest-api-with-authentication-9733cb5f51dc)
