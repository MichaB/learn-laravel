---
layout: pages/post.pug
title: Pass data from Laravel to Vue
category: advanced
description: How to pass data from Laravel blade template to Vue component.
unsplashImageId: hpUDtMgq72A
isHomeTeaser: 3
---
[Pass data from Laravel to Vue](https://codersdiaries.com/pass-data-laravel-vue/amp/)
