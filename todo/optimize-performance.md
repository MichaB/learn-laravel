---
layout: pages/post.pug
title: Optimize performance
category: basic
description: Performance is a great deal in any application. Everybody wants their system to be fast. No company can boost its business without a proper system with good performance.
unsplashImageId: LYZJXCyKCa0
---
[Optimize performance](https://www.kodementor.com/optimize-performance-of-laravel-application/)
