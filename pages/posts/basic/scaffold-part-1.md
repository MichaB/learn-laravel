---
layout: pages/post.pug
title: Scaffold Part 1
category: basic
description: We will create a scaffold with Laravel 5.7 to using like base for our future projects. The first step is learning to create a authentication system.
unsplashImageId: llkPqjQTeok
isHomeTeaser: 1
isHomeSlide: 2
---

[Scaffold - Part 1](https://medium.com/modulr/create-scaffold-with-laravel-5-7-f5ab353dff1c)

### Installation
```bash
# If laravel installer is not already available
composer global require laravel/installer

laravel new project-name
```

### Enable authentication
```bash
cd into/project-name
php artisan make:auth
```

### Create database
```bash
mysql -u {root} -p
mysql> create database `laravel-scaffold`;
```

### Configure `.env`
```bash
vim .env
```

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel-scaffold
DB_USERNAME=user
DB_PASSWORD=password
```

### Create tables
```bash
php artisan migrate
```

### Generate user table seed
```bash
php artisan make:seeder UsersTableSeeder
```

**`database/seeds/UsersTableSeeder.php`**
```php
public function run()
{
  factory(App\User::class, 50)->create();
}
```

**`database/seeds/DatabaseSeeder.php`**
```php
public function run()
{
  $this->call(UsersTableSeeder::class);
}
```
```bash
php artisan db:seed
```

### Install Vuejs Frontend
```bash
npm install
```

### Add BrowserSync
**`webpack.mix.js`**
```javascript
mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .browserSync('laravel-scaffold.test');
```

### Compiling assets
```bash
npm run dev

# or
npm run watch
```

### Start dev server
```bash
php artisan serve

# or with specified port
php artisan serve --port 4000
```
