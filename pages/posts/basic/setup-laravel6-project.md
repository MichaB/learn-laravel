---
layout: pages/post.pug
title: Setup Laravel 6 project
category: basic
description: Learn how to setup a Laravel 6 project.
unsplashImageId: KMn4VEeEPR8
isHomeTeaser: 5
---
### Use Docker as an environment
I prefer to use my own Docker environment, so I created this one:  
[Docker Laravel Advanced](https://github.com/seebaermichi/docker-laravel-advanced)

Just use it as described in the `README.md` file.

### Adjust database settings
Change the database settings in the `.env` file as follows:
```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=laravel_app
DB_USERNAME=root
DB_PASSWORD=password
```

### `make:auth` in Laravel 6
Add the laravel/ui package:
```bash
composer require laravel/ui --dev
```

Generate basic scaffolding:
```bash
php artisan ui vue
```

Generate login / registration scaffolding
```bash
php artisan ui vue --auth
```

Install all frontend dependencies
```bash
npm insall && npm run dev
```

Migrate database
```bash
docker-compose exec app php artisan migrate
```

### Enable email verification
##### Model Preparation
To get started, verify that your `App\User` model implements  
the `Illuminate\Contracts\Auth\MustVerifyEmail` contract
```php
<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    // ...
}
```

##### Routing
Register the necessary routes for verifying:
```php
Auth::routes(['verify' => true]);
```

> For further settings see [Email Verification](https://laravel.com/docs/6.x/verification)
