const Plugin = require('../plugin')

class HomeSlides extends Plugin {
    constructor(data) {
        super(data, `${__dirname}/config/home-slides.yaml`)
    }

    addMetaData() {
        if (this.pagesData !== null && Array.isArray(this.pagesData)) {
            const homeSlides = []

            this.pagesData.forEach(({ meta }) => {
                if (meta[this.configData.metaPropertyName]) {
                    homeSlides.push({
                        category: meta.category,
                        description: meta.description,
                        href: meta.htmlPathName,
                        position: meta[this.configData.metaPropertyName],
                        title: meta.title
                    })
                }
            })

            this.pagesData = this.pagesData.map(dataSet => {
                if (dataSet.meta.htmlPathName === '/index.html') {
                    return {
                        content: dataSet.content,
                        meta: Object.assign({}, dataSet.meta, {
                            homeSlides: homeSlides.sort((a, b) => a.position - b.position)
                        })
                    }
                }
                return dataSet
            })
        }
        return this.pagesData
    }
}

module.exports = HomeSlides
