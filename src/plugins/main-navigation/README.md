# Main navigation
## Plugin for [nera](https://github.com/seebaermichi/nera) static site generator
This is a small plugin, which enables a simple main navigation in nera.

It takes the elements of the `./config/main-navigation.yaml` pushes them    
into the app object of nera so that they can be rendered in the main  
navigation.
