---
layout: pages/home.pug
title: Home
---
# Learn Laravel
This is a page which should help me to learn Laravel.

## Table of contents
### Basics
* [Scaffold - Part 1](https://medium.com/modulr/create-scaffold-with-laravel-5-7-f5ab353dff1c)
* [Scaffold - Part 2](https://medium.com/modulr/create-scaffold-with-laravel-5-7-add-core-ui-template-part-2-d5263da689bb)
* [How to add custom route file](https://m.youtube.com/watch?feature=youtu.be&v=WoGqCk0pppk)
* [Tinker shell](https://laravel-news.com/laravel-tinker)
* [Tinker with data](https://scotch.io/tutorials/tinker-with-the-data-in-your-laravel-apps-with-php-artisan-tinker)
* [Optimize performance](https://www.kodementor.com/optimize-performance-of-laravel-application/)
* [Pushing Laravel further](https://medium.com/@alexrenoki/pushing-laravel-further-best-tips-good-practices-for-laravel-5-7-ac97305b8cac)

### Advanced
* [Introduction to JSON Api](https://laravel-news.com/json-api-introduction)
* [Pass data from Laravel to Vue](https://codersdiaries.com/pass-data-laravel-vue/amp/)
* [File upload](https://quickadminpanel.com/blog/file-upload-in-laravel-the-ultimate-guide/)
* [Stripe Payment Gateway integration](https://appdividend.com/2018/12/05/laravel-stripe-payment-gateway-integration-tutorial-with-example/)

### Tutorials
* [SPA with role-based authentication](https://medium.com/@ripoche.b/create-a-spa-with-role-based-authentication-with-laravel-and-vue-js-ac4b260b882f)
* [E-Mail verification](https://demonuts.com/laravel-5-8-email-verification/)
* [Live search feature](https://pusher.com/tutorials/search-laravel-vue)
* [Import CSV](https://github.com/LaravelDaily/Laravel-Import-CSV-Demo)
* [Force password change](https://laraveldaily.com/password-expired-force-change-password-every-30-days/)
* [REST Api with passport authentication](https://medium.com/@abdoumjr/laravel-passport-create-rest-api-with-authentication-9733cb5f51dc)

### Apps
* [CMS](https://code.tutsplus.com/articles/newly-updated-course-build-a-cms-with-laravel--cms-30386)
* [e-commerce application - Part 1](https://blog.pusher.com/ecommerce-laravel-vue-part-1/)

## Packages
* [reCaptcha](https://laravel-news.com/recaptcha-packages-for-laravel)

## Sources
* [Official documentation](https://laravel.com/)
* [Laravel news](https://laravel-news.com/)
* [Laravel collections](https://laravelcollections.com/)
