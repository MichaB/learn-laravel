---
layout: pages/post.pug
title: SPA with role-based authentication
category: tutorial
description: Implement a SPA (Single Page Application) with a role-based authentication with Laravel and Vue.js.
unsplashImageId: 5SGZCmLsN0U
isPopularPost: 1
---
[SPA with role-based authentication](https://medium.com/@ripoche.b/create-a-spa-with-role-based-authentication-with-laravel-and-vue-js-ac4b260b882f)
