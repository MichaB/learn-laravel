---
layout: pages/post.pug
title: Tinker shell
category: basic
description: Laravel includes a powerful REPL, called Tinker, powered by the PsySH console by Justin Hileman under the hood. The tinker console allows you to interact with your Laravel application from the command line in an interactive shell.
unsplashImageId: J3ABLQjZQBg
isHomeTeaser: 2
---
[Tinker shell](https://laravel-news.com/laravel-tinker)
