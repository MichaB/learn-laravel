---
layout: pages/post.pug
title: Tinker with data
category: basic
description: How to use one of Laravel's lesser-known features to quickly read data from our Laravel applications. We can use Laravel artisan's built-in php artisan tinker to mess around with your application and things in the database.
unsplashImageId: tN-GqYSrIrM
---
[Tinker with data](https://scotch.io/tutorials/tinker-with-the-data-in-your-laravel-apps-with-php-artisan-tinker)
