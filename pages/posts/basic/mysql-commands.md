---
layout: pages/post.pug
title: MySQL commands
category: basic
description: Here we have some basic commands which are quite useful on a daily usage.
unsplashImageId: wxGwllldlIQ
---
```sql
-- Login, asks for password (recommended)
mysql -u root -p

SHOW databases;

CREATE database `database-name`;

DROP database `database-name`;

USE `database-name`;

-- Shows tables in used database
SHOW tables;

-- Delete all entries in table, doesn't reset ID
DELETE from `table-name`;

-- Delete all entries in table and reset ID
TRUNCATE table `table-name`;
```
