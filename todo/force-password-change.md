---
layout: pages/post.pug
title: Force password change
category: tutorial
description: In some systems it is required to change password every X days. Laravel doesn’t have that functionality out-of-the-box, but it’s easy to build.
unsplashImageId: eUobvePZrec
---
[Force password change](https://laraveldaily.com/password-expired-force-change-password-every-30-days/)
