---
layout: pages/post.pug
title: Content Management System
category: app
description: Want to build your own content management system (CMS) in 2018? Learn exactly how it's done in our comprehensive course Build a CMS With Laravel, freshly updated with brand new lessons to take advantage of the latest features of Laravel 5.
unsplashImageId: EZSlp_sVXzM
---
[CMS](https://code.tutsplus.com/articles/newly-updated-course-build-a-cms-with-laravel--cms-30386)
