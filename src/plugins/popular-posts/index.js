const moment = require('moment')
const Plugin = require('../plugin')

class PopularPosts extends Plugin {
    constructor(data) {
        super(data, `${__dirname}/config/popular-posts.yaml`)
    }

    addMetaData() {
        if (this.pagesData !== null && Array.isArray(this.pagesData)) {
            const popularPosts = []

            this.pagesData.forEach(({ meta }) => {
                if (meta[this.configData.metaPropertyName]) {
                    popularPosts.push({
                        createdAt: moment(meta.createdAt).fromNow(),
                        image: meta.unsplashImageId,
                        href: meta.htmlPathName,
                        position: meta[this.configData.metaPropertyName],
                        title: meta.title
                    })
                }
            })

            this.pagesData = this.pagesData.map(dataSet => {
                if (dataSet.meta.htmlPathName === '/index.html') {
                    return {
                        content: dataSet.content,
                        meta: Object.assign({}, dataSet.meta, {
                            popularPosts: popularPosts.sort((a, b) => a.position - b.position)
                        })
                    }
                }
                return dataSet
            })
        }
        return this.pagesData
    }
}

module.exports = PopularPosts
