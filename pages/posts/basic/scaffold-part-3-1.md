---
layout: pages/post.pug
title: Scaffold Part 3.1
category: basic
description: In this part we will add to profile module the feature edit the name and email of Auth user.
unsplashImageId: KMn4VEeEPR8
---
[Scaffold - Part 3.1](https://medium.com/modulr/create-scaffold-with-laravel-5-7-profile-part-3-1-edit-profile-344342a65ff5)

### Create routes
```bash
# create routes file
mkdir routes/profile

cd routes/profile
touch profile.php
```

**`routes/profile/profile.php`**
```php
<?php
Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'Profile'], function() {
        // view    
        Route::view('/profile', 'profile.profile');
        // api
        Route::group(['prefix' => 'api/profile'], function() {
            Route::get('/getAuthUser',
                       'ProfileController@getAuthUser');
            Route::put('/updateAuthUser',
                       'ProfileController@updateAuthUser');
        });
    });
});
```

**`routes/web.php`**
```php
...
Route::get('/dashboard', 'HomeController@index')->name('home');

require __DIR__ . '/profile/profile.php';
```

### Create Controller
```bash
php artisan make:controller Profile/ProfileController
```

**`app/Http/Controllers/Profile/ProfileController.php`**
```php
...
use Illuminate\Support\Facades\Auth;
use App\User;

...

public function getAuthUser ()
{
    return Auth::user();
}

public function updateAuthUser (Request $request)
{
    $this->validate($request, [
        'name' => 'required|string',
        'email' => 'required|email|unique:users,email,'.Auth::id()
    ]);

    $user = User::find(Auth::id());

    $user->name = $request->name;
    $user->email = $request->email;
    $user->save();

    return $user;
}
```

### Create View
```bash
# create view
mkdir resources/views/profile

cd resources/views/profile
touch profile.blade.php
```

**`resources/views/profile/profile.blade.php`**
```html
@extends('layouts.app')

@section('header')
    @include('layouts.header')
@endsection

@section('sidebar')
    @include('layouts.sidebar')
@endsection

@section('content')
<div class="py-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <profile></profile>
        </div>
    </div>
</div>
@endsection
```

### Create Vuejs component
```bash
# create component
mkdir resources/js/components/profile

cd resources/js/components/profile
touch Profile.vue
```

**`resources/js/components/profile/Profile.vue`**
```html
<template>
    <div class="card">
        <div class="card-header">
            <i class="fas fa-pencil-alt"></i> Edit Profile
        </div>
        <div class="card-body">
            <form class="form-horizontal">
                <div class="form-group row">
                    <label class="col-md-3">Full Name</label>
                    <div class="col-md-9">
                        <input class="form-control" :class="{'is-invalid': errors.name}" type="text"
                               v-model="user.name">
                        <span class="help-block">Enter your name, so people you know can recognize you.</span>
                        <div class="invalid-feedback" v-if="errors.name">{{errors.name[0]}}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3">Email</label>
                    <div class="col-md-9">
                        <input class="form-control" :class="{'is-invalid': errors.email}" type="email"
                               v-model="user.email">
                        <span class="help-block">This email will be displayed on your public profile.</span>
                        <div class="invalid-feedback" v-if="errors.email">{{errors.email[0]}}</div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <div class="form-group row">
                <div class="col-md-9 offset-md-3">
                    <button class="btn btn-primary" type="button" :disabled="submiting" @click="updateAuthUser">
                        <i class="fas fa-spinner fa-spin" v-if="submiting"></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>
</template>

<script>
    export default {
        data() {
            return {
                user: {},
                errors: {},
                submiting: false
            }
        },
        mounted() {
            this.getAuthUser()
        },
        methods: {
            getAuthUser() {
                axios.get(`/api/profile/getAuthUser`)
                    .then(response => {
                        this.user = response.data
                    })
            },
            updateAuthUser() {
                this.submiting = true
                axios.put(`/api/profile/updateAuthUser`, this.user)
                    .then(response => {
                        this.errors = {}
                        this.submiting = false
                        this.$toasted.global.error('Profile updated!')
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors
                        this.submiting = false
                    })
            }
        }
    }
</script>
```

**`resources/js/app.js`**
```javascript
...
window.Vue = require('vue');
Vue.component('profile', require('./components/profile/Profile.vue').default);

const app = new Vue({
    el: '#app'
});
```

To send notifications we use the [Vue Toasted](https://github.com/shakee93/vue-toasted) Vuejs plugin.
```bash
npm i -S vue-toasted
```

**`resources/js/app.js`**
Register global plugin
```javascript
...
window.Vue = require('vue');
import Toasted from 'vue-toasted';
Vue.use(Toasted)
Vue.toasted.register('error', message => message, {
    position : 'bottom-center',
    duration : 1000
})
Vue.component('profile', require('./components/profile/Profile.vue').default);
...
```

### Compile assets
```bash
npm run dev
```
