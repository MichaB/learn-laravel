const Plugin = require('../plugin')

class CategoryPostLinks extends Plugin {
    addMetaData() {
        if (this.pagesData !== null && Array.isArray(this.pagesData)) {
            const categoryPostLinks = []

            this.pagesData.forEach(({ meta }) => {
                const pagePathElements = meta.pagePathName.split('/')

                if (pagePathElements[0] === 'posts') {
                    categoryPostLinks.push({
                        category: pagePathElements[1],
                        createdAt: meta.createdAt,
                        href: meta.htmlPathName,
                        name: meta.title
                    })
                }
            })

            this.pagesData = this.pagesData.map(dataSet => {
                if (dataSet.meta.pagePathName.split('/')[0] === 'categories') {
                    return {
                        content: dataSet.content,
                        meta: Object.assign({}, dataSet.meta, {
                            categoryPostLinks: categoryPostLinks.filter(({ category }) => dataSet.meta.htmlPathName.includes(category))
                                .sort((a, b) => {
                                    if (a.createdAt <= b.createdAt) return -1
                                    if (a.createdAt > b.createdAt) return 1
                                    return 0
                                })
                        })
                    }
                }

                return dataSet
            })
        }

        return this.pagesData
    }
}

module.exports = CategoryPostLinks
