const Plugin = require('../plugin')

class HomeTeasers extends Plugin {
    constructor(data) {
        super(data, `${__dirname}/config/home-teasers.yaml`)
    }

    addMetaData() {
        if (this.pagesData !== null && Array.isArray(this.pagesData)) {
            const homeTeasers = []

            this.pagesData.forEach(({ meta }) => {
                if (meta[this.configData.metaPropertyName]) {
                    homeTeasers.push({
                        category: meta.category,
                        description: meta.description,
                        image: meta.unsplashImageId,
                        href: meta.htmlPathName,
                        position: meta[this.configData.metaPropertyName],
                        title: meta.title
                    })
                }
            })

            this.pagesData = this.pagesData.map(dataSet => {
                if (dataSet.meta.htmlPathName === '/index.html') {
                    return {
                        content: dataSet.content,
                        meta: Object.assign({}, dataSet.meta, {
                            homeTeasers: homeTeasers.sort((a, b) => b.position - a.position)
                        })
                    }
                }
                return dataSet
            })
        }
        return this.pagesData
    }
}

module.exports = HomeTeasers
