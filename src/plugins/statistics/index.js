const Plugin = require('../plugin')

class Statistics extends Plugin {
    addAppData() {
        const statistics = {}

        if (this.pagesData !== null && Array.isArray(this.pagesData)) {
            statistics.amountOfCategories = []

            this.pagesData.forEach(({ meta }) => {
                if (meta.category) {
                    if (statistics.amountOfCategories.some(({ category }) => category === meta.category)) {
                        statistics.amountOfCategories = statistics.amountOfCategories.map(item => {
                            if (item.category === meta.category) {
                                return {
                                    category: meta.category,
                                    amount: item.amount + 1
                                }
                            } else {
                                return item
                            }
                        })
                    } else {
                        statistics.amountOfCategories.push({
                            category: meta.category,
                            amount: 1
                        })
                    }
                }
            })
        }

        return Object.assign({}, this.appData, {
            statistics
        })
    }
}

module.exports = Statistics
