---
layout: pages/post.pug
title: How to add a custom route file
category: basic
description: Learn how to create custom Laravel Route files. Laravel makes adding routes to your application super easy, but sometimes your route files get a litter cluttered or large.
unsplashImageId: vot1mpfUwZA
---
[How to add custom route file](https://m.youtube.com/watch?feature=youtu.be&v=WoGqCk0pppk)
