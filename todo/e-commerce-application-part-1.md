---
layout: pages/post.pug
title: e-commerce application - Part 1
category: app
description: In this article, we will cover how you can use Laravel to build a simple e-commerce application. After this tutorial, you should know how to use Laravel and Vue to make a web application and understand the basics of making an online store.
unsplashImageId: dB_febOU-I0
---
[e-commerce application - Part 1](https://blog.pusher.com/ecommerce-laravel-vue-part-1/)
