---
layout: pages/post.pug
title: Pushing Laravel further
category: basic
description: Laravel is already known by many PHP developers for writing clean, working and debugg-able code. It also has support for many many features, that sometimes aren’t listed in the docs, or they were, but they were removed for various reasons.
unsplashImageId: ZuZK8D55_cw
isPopularPost: 2
---
[Pushing Laravel further](https://medium.com/@alexrenoki/pushing-laravel-further-best-tips-good-practices-for-laravel-5-7-ac97305b8cac)
