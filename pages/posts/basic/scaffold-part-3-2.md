---
layout: pages/post.pug
title: Scaffold Part 3.2
category: basic
description: In this part we will add to profile module the feature change password of Auth user.
unsplashImageId: 3V8gdLbwDOI
---
[Scaffold - Part 3.2](https://medium.com/modulr/create-scaffold-with-laravel-5-7-profile-part-3-2-change-password-5fd00d07283)

### Add routes
**`routes/profile/profile.php`**
```php
<?php
Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'Profile'], function() {
        // view    
        Route::view('/profile', 'profile.profile');
        Route::view('/password', 'profile.password');
        // api
        Route::group(['prefix' => 'api/profile'], function() {
            Route::get('/getAuthUser',
                       'ProfileController@getAuthUser');
            Route::put('/updateAuthUser',
                       'ProfileController@updateAuthUser');
            Route::put('/updateAuthUserPassword',
                       'ProfileController@updateAuthUserPassword');
        });
    });
});
```

### Update controller
**`app/Http/Controllersr/Pofile/ProfileController.php`**
```php
...
use Illuminate\Support\Facades\Hash;

...

public function updateAuthUserPassword(Request $request)
{
    $this->validate($request, [
        'current' => 'required',
        'password' => 'required|confirmed',
        'password_confirmation' => 'required'
    ]);

    $user = User::find(Auth::id());

    if (!Hash::check($request->current, $user->password)) {
        return response()->json(['errors' => ['current'=> ['Current password does not match']]], 422);
    }

    $user->password = Hash::make($request->password);
    $user->save();

    return $user;
}
```

### Create view
```bash
# create view
touch resources/views/profile/password.blade.php
```

**`resources/views/profile/password.blade.php`**
```html
@extends('layouts.app')
@section('header')
    @include('layouts.header')
@endsection
@section('sidebar')
    @include('layouts.sidebar')
@endsection
@section('content')
<div class="py-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <profile-password></profile-password>
        </div>
    </div>
</div>
@endsection
```

### Create Vuejs component
```bash
# create component
touch resources/js/components/profile/Password.vue
```

**`resources/js/components/profile/Password.vue`**
```html
<template>
    <div class="card">
        <div class="card-header">
            <i class="fas fa-pencil-alt"></i> Edit Password
        </div>
        <div class="card-body">
            <form class="form-horizontal">
                <div class="form-group row">
                    <label class="col-md-3">Current password</label>
                    <div class="col-md-9">
                        <input class="form-control" :class="{'is-invalid': errors.current}" type="password"
                               v-model="password.current">
                        <span class="help-block">You must provide your current password in order to change it.</span>
                        <div class="invalid-feedback" v-if="errors.current">{{errors.current[0]}}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3">New password</label>
                    <div class="col-md-9">
                        <input class="form-control" :class="{'is-invalid': errors.password}" type="password"
                               v-model="password.password">
                        <div class="invalid-feedback" v-if="errors.password">{{errors.password[0]}}</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3">Password confirmation</label>
                    <div class="col-md-9">
                        <input class="form-control" :class="{'is-invalid': errors.password_confirmation}"
                               type="password" v-model="password.password_confirmation">
                        <div class="invalid-feedback" v-if="errors.password_confirmation">
                            {{errors.password_confirmation[0]}}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <div class="form-group row">
                <div class="col-md-9 offset-md-3">
                    <button class="btn btn-primary" type="button" :disabled="submiting" @click="updateAuthUserPassword">
                        <i class="fas fa-spinner fa-spin" v-if="submiting"></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>
</template>

<script>
    export default {
        data() {
            return {
                password: {},
                errors: {},
                submiting: false
            }
        },
        methods: {
            updateAuthUserPassword() {
                this.submiting = true
                axios.put(`/api/profile/updateAuthUserPassword`, this.password)
                    .then(response => {
                        this.password = {}
                        this.errors = {}
                        this.submiting = false
                        this.$toasted.global.error('Password changed!')
                    })
                    .catch(error => {
                        this.errors = error.response.data.errors
                        this.submiting = false
                    })
            }
        }
    }
</script>
```

**`resources/js/app.js`**
```javascript
...
window.Vue = require('vue');
Vue.component('profile', require('./components/profile/Profile.vue').default);
Vue.component('profile-password', require('./components/profile/Password.vue').default);
const app = new Vue({
    el: '#app'
});
```

### Compile assets
```bash
npm run dev
```
